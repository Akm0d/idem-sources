import pop.hub
import pytest
from pytest_idem.runner import IdemRunException
from pytest_idem.runner import run_sls


@pytest.mark.parametrize("secondary_protocol", ["http", "https"])
def test_cache(hub, secondary_protocol):
    ret = run_sls(
        ["sls.test"],
        sls_sources=[
            f"git+{secondary_protocol}://gitlab.com/vmware/idem/sls-store.git"
        ],
        hard_fail_on_collect=True,
    )
    assert len(ret) == 14
    assert all("test" in key for key in ret.keys())


@pytest.mark.parametrize("secondary_protocol", ["http", "https"])
def test_missing(hub, secondary_protocol):
    with pytest.raises(IdemRunException) as e:
        run_sls(
            ["non_existent_sls"],
            sls_sources=[
                f"git+{secondary_protocol}://gitlab.com/vmware/idem/sls-store.git"
            ],
            hard_fail_on_collect=True,
        )
        assert e == ""


@pytest.mark.parametrize("secondary_protocol", ["http", "https"])
def test_bad_url(hub, secondary_protocol):
    with pytest.raises(IdemRunException) as e:
        run_sls(
            ["non_existent_sls"],
            sls_sources=[
                f"git+{secondary_protocol}://gitlab.com/vmware/idem/sls-store-fake.git"
            ],
            hard_fail_on_collect=False,
        )
        assert e == "SLS ref 'non_existent_sls' did not resolve from sources"


def test_plain_url(hub):
    with pytest.raises(IdemRunException) as e:
        run_sls(
            ["non_existent_sls"],
            sls_sources=["git://github.com/moremobans/pypi-mobans.git!/templates"],
            hard_fail_on_collect=False,
        )
        assert e == "SLS ref 'non_existent_sls' did not resolve from sources"


@pytest.mark.parametrize("secondary_protocol", ["http", "https", "git"])
def test_ctx(secondary_protocol: str):
    acct_data = {
        "profiles": {
            "git": {
                "my_profile": {
                    "username": "my_user",
                    "password": "my_pass",
                    "other_value": "my_val",
                }
            }
        }
    }
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idem")
    mock_hub = hub.pop.testing.mock_hub()
    hub.source.git.clone = mock_hub.source.git.clone
    hub.source.git.build_ssh_command = mock_hub.source.git.build_ssh_command
    hub.source.git.build_git_url = mock_hub.source.git.build_git_url
    mock_hub.source.git.build_ssh_command.return_value = "mock ssh"
    mock_hub.source.git.build_git_url.return_value = "mock url"

    with pytest.raises(IdemRunException):
        run_sls(
            ["sls.test"],
            sls_sources=[
                f"git+{secondary_protocol}://my_profile@gitlab.com/vmware/idem/sls-store.git"
            ],
            hard_fail_on_collect=False,
            acct_data=acct_data,
            hub=hub,
        )

    mock_hub.source.git.clone.assert_called_with(
        {
            "acct": {
                "username": "my_user",
                "password": "my_pass",
                "other_value": "my_val",
            }
        },
        url="mock url",
        env={"GIT_SSH_COMMAND": "mock ssh"},
    )

    mock_hub.source.git.build_ssh_command.assert_called_with(
        {
            "acct": {
                "username": "my_user",
                "password": "my_pass",
                "other_value": "my_val",
            }
        }
    )
    mock_hub.source.git.build_git_url.assert_called_with(
        {
            "acct": {
                "username": "my_user",
                "password": "my_pass",
                "other_value": "my_val",
            }
        },
        protocol=f"git+{secondary_protocol}",
        source="gitlab.com/vmware/idem/sls-store.git",
    )
